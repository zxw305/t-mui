# Search

#### 用法

```vue
import { Search } from 'ftk-mui'
<Search placeholder="请输入" @confirm="searchConfrim">
  // 内部slot为左侧栏，可自定义传入，如取消按钮等
</Search>
```

Search的属性说明如下：

| 属性           | 说明                       | 类型     | 可选值        | 默认值     |
| :------------- | -------------------------- | -------- | ------------- | ---------- |
| width          | 搜索框宽度                 | String   |               | 2rem       |
| height         | 搜索框高度                 | String   |               | 0.25rem    |
| clearButton    | 是否显示清除按钮           | Boolean  | true \| false | true       |
| top            | 清除按钮位于搜索框告诉     | String   |               | 0.06rem    |
| placeholder    | placeholder                | String   |               |            |
| bgColor        | 搜索框背景色               | String   |               |            |
| borderColor    | 搜索框border颜色           | String   |               | \#eee      |
| radius         | 搜索框圆角                 | String   |               | 0.2rem     |
| searchStyle    | 搜索框内搜索标志自定义样式 | Object   |               |            |
| searchBtnStyle | 右侧搜索按钮自定义样式     | Object   |               |            |
| searchBtn      | 是否需要右侧搜索按钮       | Boolean  | true \| false | true       |
| clear          | 清除按钮点击后触发事件     | Function |               |            |
| confirm        | 确认搜索按钮点击事件       | Function |               | 返回输入值 |
| focus          | 聚焦事件                   | Function |               |            |
| input          | value 改变时触发事件       | Function |               | 返回输入值 |

**插槽说明：**

​       search自带两个slot,可自定义中间搜索框两侧内容:

搜索框左侧： name=search-left

搜索框右侧： name=search-right  （右侧slot与搜索确认按钮只可其一，如自定义搜索框右侧内容，则自实现确认搜索事件）

**使用：**

```
<Search placeholder="请输入" width="2.6rem" :searchStyle="searchStyle" :searchBtnStyle="searchBtnStyle"  @confirm="searchConfrim">
    <span slot="search-left" class="iconfont iconrt iconBack" @click="searchConfrim"></span>
    <span slot="search-right" class="iconfont iconccgl-shangjiasaomiao-6" @click="searchConfrim"></span>
</Search>
```



示例默认样式:

![image-20200820115927026](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200820115927026.png)

#### 使用样例1：

```vue
<Search placeholder="请输入" width="2.6rem" :searchStyle="searchStyle" :searchBtnStyle="searchBtnStyle"  @confirm="searchConfrim">
	// 左侧返回
	<span class="iconfont iconrt iconBack" @click="searchConfrim"></span>
</Search>
searchBtnStyle: {
    color: '#000',
    background: "#fff",
    width: '0.3rem'
},
searchStyle: {
    color: '#ddd',
    // 'font-size': '0.13rem'
},

.iconBack {
  margin-right: 0.03rem;
  font-size: 0.22rem;
  color: #808080;
}
```

![image-20200820120007426](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200820120007426.png)

#### 自定义搜索框两侧使用样例2：

```vue
<Search placeholder="请输入" width="2.6rem" :searchStyle="searchStyle" :searchBtn="false">
	<span slot="search-left" class="iconfont iconrt iconBack" @click="searchConfrim"></span>
	<span slot="search-right" class="iconfont icontianjia icongo" @click="searchConfrim"></span>
</Search>
```

![image-20200820135220145](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200820135220145.png)