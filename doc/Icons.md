# Icons

#### 用法

```vue
import { Icons } from 'ftk-mui'

<Icons type="iconshezhi" :alignStyle="iconStyle"/>

iconStyle: {
  'color': 'red'
},
```

字体文件的属性说明如下：

| 属性       | 说明           | 类型   | 可选值 | 默认值 |
| :--------- | -------------- | ------ | ------ | ------ |
| type       | 字体类型       | String | 如下   |        |
| alignStyle | 字体自定义样式 | Object |        |        |

##### type可选如下：

| 字体名                     | 释义     | 图样                                                         |
| -------------------------- | -------- | ------------------------------------------------------------ |
| iconshezhi                 | 设置     | ![image-20200722145046849](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145046849.png) |
| iconsousuo-011             | 搜索     | ![image-20200722145055233](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145055233.png) |
| icon1_xiaoshoudaibiao      | 人员设置 | ![image-20200722145101465](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145101465.png) |
| icon1_mima                 | 密码     | ![image-20200722145116839](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145116839.png) |
| icon1_yonghu               | 用户     | ![image-20200722145123741](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145123741.png) |
| icon1_dangqianbanben       | 当前版本 | ![image-20200722145132506](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145132506.png) |
| icon1_guanyuwomen          | 关于我们 | ![image-20200722145139601](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145139601.png) |
| icon1_yinsixieyizhengce    | 隐私协议 | ![image-20200722145144918](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145144918.png) |
| icon1_fuwuxieyi            | 服务协议 | ![image-20200722145150540](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145150540.png) |
| icon1_tuichu               | 退出     | ![image-20200722145156343](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145156343.png) |
| iconrili                   | 日历     | ![image-20200722145205435](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145205435.png) |
| iconccgl-shangjiasaomiao-6 | 扫描     | ![image-20200722145210551](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145210551.png) |
| iconarrow-left-copy        | 左箭头   | ![image-20200722145215627](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145215627.png) |
| iconrt                     | 右箭头   | ![image-20200722145226956](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145226956.png) |
| iconyulan                  | 预览     | ![image-20200722145237675](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145237675.png) |
| icontianjia                | 加号     | ![image-20200722145243625](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145243625.png) |
| iconshangchuan             | 上传     | ![image-20200722145249540](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145249540.png) |
| iconxiazai                 | 下载     | ![image-20200722145256299](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145256299.png) |
| iconshipinmianqian         | 视频     | ![image-20200722145302053](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145302053.png) |
| iconshenqingdaikuan        | 钱包     | ![image-20200722145307612](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145307612.png) |
| iconshenhechaxun           | 审核     | ![image-20200722145312610](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145312610.png) |
| iconwode-01                | 我的     | ![image-20200722145333508](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145333508.png) |
| iconshouye-01              | 首页     | ![image-20200722145326992](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200722145326992.png) |

