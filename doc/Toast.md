# Toast

#### 用法

```vue
import { Toast } from 'ftk-mui'
<Toast :visible.sync="show2" :during="3" msg="系统异常"></Toast>
```

toast提示的属性说明如下：

| 属性    | 说明         | 类型    | 可选值                                                       | 默认值 |
| :------ | ------------ | ------- | ------------------------------------------------------------ | ------ |
| visible | 显示隐藏     | Boolean | true \| false                                                | false  |
| during  | 提示的时间长 | Number  |                                                              | 2s     |
| msg     | 提示语       | String  |                                                              |        |
| type    | toast类型    | String  | 具体如下                                                     | text   |
|         | text         | String  | ![image-20200810154930354](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810154930354.png) |        |
|         | success      | String  | ![image-20200810155152044](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810155152044.png) |        |
|         | fail         | String  | ![image-20200810155207756](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810155207756.png) |        |
|         | warning      | String  | ![image-20200810155239179](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810155239179.png) |        |
|         | loading      | String  | ![image-20200810155301619](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810155301619.png) |        |

#### 使用实例

```vue
import { Head, Form, Toast, loading } from 'ftk-mui'
<!-- toast提示 -->
<Toast :visible.sync="showtoast" :during="3" :msg="toastMsg" ref="toastCommen"></Toast>
```

![image-20200810172151023](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810172151023.png)