# Loading

#### 用法

```vue
import { Loading } from 'ftk-mui'
<Loading :shadeShow="true" :visible="loadingVisible" />
```

Loading的属性说明如下：

| 属性      | 说明           | 类型    | 可选值        | 默认值 |
| :-------- | -------------- | ------- | ------------- | ------ |
| visible   | 显示隐藏       | Boolean | true \| false | false  |
| shadeShow | 是否显示遮罩层 | Boolean | true \| false | true   |
| color     | loading主题色  | String  |               |        |

默认样式

![image-20200810162533248](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810162533248.png)

#### 使用样例

```
import { Head, Form, Toast, loading } from 'ftk-mui'
<!-- Loading -->
<Loading :visible="loadingVisible" color="#006DBB"/>
```

样式：

![image-20200810163232857](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810163232857.png)

