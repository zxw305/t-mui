# ActionSheet

#### 用法

```vue
import { ActionSheet } from 'ftk-mui'

<ActionSheet :visible.sync="actionShow" :dataList="testAction"></ActionSheet>
actionShow: true,
testAction: ['1', '3', '6', '8'],
```

ActionShee的属性说明如下：

| 属性      | 说明         | 类型    | 可选值 | 默认值 |
| :-------- | ------------ | ------- | ------ | ------ |
| visible   | 是否显示     | Boolean |        |        |
| dataList  | 值数组       | Array   | 必传   |        |
| shadeShow | 是否显示遮罩 | Boolean |        | true   |

示例样式:

![image-20200818103012436](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200818103012436.png)