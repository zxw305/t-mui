# BackTop

#### 用法

```vue
import { BackTop } from 'ftk-mui'

<BackTop :alignStyle="alignStyleBack"></BackTop>

```

BackTop的属性说明如下：

| 属性       | 说明                     | 类型   | 可选值 | 默认值 |
| :--------- | ------------------------ | ------ | ------ | ------ |
| distance   | 距离顶部距离以显示按钮   | Number |        | 100    |
| duration   | 按钮延时                 | Number |        | 50ms   |
| alignStyle | 按钮样式，可自定义位置等 | Object |        |        |

示例样式:

![image-20200818140059874](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200818140059874.png)