# Button

#### 用法

```vue
import { Head, Button } from 'ftk-mui'

<Button @btnclick="test">演示</Button>
```

按钮的属性说明如下：

| 属性          | 说明             | 类型            | 默认值 |
| ------------- | ---------------- | --------------- | ------ |
| disabled      | 按钮失效状态     | boolean         | false  |
| btnclick      | 点击按钮时的回调 | (event) => void |        |
| btnAlignStyle | 按钮自定义样式   | Obeject         |        |

默认样式： 

![image-20200721174046740](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200721174046740.png)

#### 使用实例

```vue
<Button :btnAlignStyle="btnStyle" @btnclick="toSettlement">提前结清</Button>
// btn配置
btnStyle: {
    'position': 'fixed',
    'bottom': '0',
    'left': '0',
    'width': '100%',
    'text-align': 'center',
    'color': '#fff',
    'background': '#006DBB'
}
```



![image-20200721173124416](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200721173124416.png)