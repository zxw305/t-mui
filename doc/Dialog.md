# Dialog

#### 用法

```vue
import { Head, Button, Dialog } from 'ftk-mui'
<Dialog
    :visible.sync="dialogShow"
    :option="tipOption"
    @onConfirm="operateShow"
    @onCancel="pickerCancel"
></Dialog>
tipOption: {
	// confirm 带确认和取消按钮的
	// tips（默认）  只带确认按钮的
	// type: 'confirm',
	title: "提示",
	message: "111234444"
	// cancelText: '回退',
	// okText: '确认'
	// okColor: '#108C3E',
	// cancelColor: 'red'
},
```

dialog提示框的属性说明如下：

| 属性      | 说明         | 类型     | 可选值          | 默认值 |
| :-------- | ------------ | -------- | --------------- | ------ |
| visible   | 显示隐藏     | Boolean  | true \| false   | false  |
| onConfirm | 确认按钮事件 | Function | center\|left    | center |
| onCancel  | 取消按钮事件 | Function |                 |        |
| option    | dialog设置   | Object   | 具体如下        |        |
|           | type         | String   | tips \| confirm | tips   |
|           | title        | String   | 标题            |        |
|           | message      | String   | 提示语          |        |
|           | cancelText   | String   | 取消按钮文字    |        |
|           | cancelColor  | String   | 取消按钮颜色    |        |
|           | okText       | String   | 确认按钮文字    |        |
|           | okColor      | String   | 确认按钮颜色    |        |

默认样式：

![image-20200810161924825](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810161924825.png)

#### 使用样例

```vue
import { Head, Button, Dialog } from 'ftk-mui'
<Dialog
    :visible.sync="dialogShow"
    :option="tipOption"
></Dialog>
tipOption: {
	title: '提示',
	message: '您当前存在未结清业务，暂不支持销户操作！',
	okColor: '#006DBB'
}
```

![image-20200810162101424](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810162101424.png)