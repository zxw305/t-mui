# Calendar

#### 用法

```vue
import { Calendar } from 'ftk-mui'

<Calendar :visible.sync="visiblecalender" confirmColor="#108C3E" cancelColor="#626262" :calendarYear="2019" :calendarMonth="1" @confirm="getDate"></Calendar>

```

ActionShee的属性说明如下：

| 属性          | 说明                       | 类型     | 可选值 | 默认值         |
| :------------ | -------------------------- | -------- | ------ | -------------- |
| visible       | 日历组件显示隐藏           | Boolean  |        | false          |
| confirmColor  | 确认按钮颜色以及选中主题色 | String   |        |                |
| cancelColor   | 取消按钮颜色               | String   |        |                |
| calendarYear  | 日历开始年份               | Number   |        | 当前年份       |
| calendarMonth | 日历开始月份               | Number   |        | 当前月份       |
| cycle         | 月份跨度                   | Number   |        | 默认显示12个月 |
| confirm       | 确认按钮回调事件           | Function |        | 返回选择的日期 |
| cancel        | 取消按钮回调事件           | Function |        |                |

示例样式:

![image-20200820152650916](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200820152650916.png)