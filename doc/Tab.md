# Tab

#### 用法

```vue
import { Tab } from 'ftk-mui'

<Tab @click="tabClick" defaultActive="1" color="red">
    <TabPane tab="组件示例1" id="1" style="padding-top:0.4rem">
        <Form
              :btnOption="btnOptionSet"
              :initialData="dataSource"
              :formData="formData"
              @formSubmit="submit"
              ref="child"
              ></Form>
    </TabPane>
    <TabPane tab="组件示例2" id="2">
        <div class="card-demo">
            <FtkCard class="card-list" :title="title" :content="content" :footer="footer">
            </FtkCard>
        </div>
    </TabPane>
    <TabPane tab="组件示例3" id="3">
        <Button @btnclick="commenTest1" class="demoShow">dialog</Button>
        <Button @btnclick="commenTest2" class="demoShow">Toast</Button>
        <Button @btnclick="commenTest3" class="demoShow">Popup</Button>
        <Button @btnclick="commenTest4" class="demoShow">Loading</Button>
        <div class="list-demo">
            <List :dataSource="listSource" :alignStyle="listAlignStyle"></List>
        </div>
    </TabPane>
</Tab>
```

Tab的属性说明如下：

| 属性          | 说明        | 类型     | 可选值 | 默认值 |
| :------------ | ----------- | -------- | ------ | ------ |
| click         | 点击事件    | Function |        |        |
| defaultActive | 默认活跃tab | String   |        |        |
| color         | tab主题色   |          |        |        |

TabPane的属性说明如下：

| 属性 | 说明    | 类型   | 可选值 | 默认值 |
| :--- | ------- | ------ | ------ | ------ |
| tab  | 标题名  | String |        |        |
| id   | tab标注 | String |        |        |



示例样式:

![image-20200817173323915](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200817173323915.png)