# Popup

#### 用法

```vue
import { Popup } from 'ftk-mui'

<Popup :shadeShow="true" :visible.sync="popupVisible" @popupClick="pickerCancel" type="bottom" :alignStyle="alignStylePopup">
      <div class="popupTest">
        <!-- eslint-disable-next-line -->
        <img
          src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591630482060&di=fd21948d2cab2e74bf9395f8bdc3ef7b&imgtype=0&src=http%3A%2F%2Fa1.att.hudong.com%2F05%2F00%2F01300000194285122188000535877.jpg"
        />
      </div>
    </Popup>
alignStylePopup: {
 background: 'red',
  height: '2rem'
},
```

Popup的属性说明如下：

| 属性       | 说明                  | 类型    | 可选值                                   | 默认值 |
| :--------- | --------------------- | ------- | ---------------------------------------- | ------ |
| visible    | 显示隐藏              | Boolean | true \| false                            | false  |
| shadeShow  | 是否显示遮罩          | Boolean | true \| false                            | true   |
| maskClose  | 点击遮罩是否关闭popup | Boolean | true \| false                            | true   |
| type       | popup从哪一侧弹出     | String  | center \| right \| left \| top \| bottom | center |
| alignStyle | 用户自定义弹出层样式  | Object  |                                          |        |
|            |                       |         |                                          |        |

**插槽说明：**

​     插槽为各方向实际内容，可自定义实现各层内容



默认示例样式:

下：

​                                                                 ![image-20200821160239163](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200821160239163.png)

上：



![image-20200821160258124](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200821160258124.png)

左;

![image-20200821160317088](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200821160317088.png)

右：

![image-20200821160335324](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200821160335324.png)

中：

![image-20200821160351232](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200821160351232.png)