# Header

#### 用法

```vue
import { Header } from 'ftk-mui'

<Header title="首页"></Header>
```

标题栏的属性说明如下：

| 属性       | 说明             | 类型     | 可选值           | 默认值  |
| :--------- | ---------------- | -------- | ---------------- | ------- |
| title      | 标题             | String   |                  |         |
| alignStyle | 标题栏自定义样式 | Object   |                  |         |
| leftText   | 左侧按钮标题     | String   |                  |         |
| rightText  | 右侧按钮标题     | String   |                  |         |
| rightIcon  | 右侧按钮图标     | String   | 见Icons type类型 |         |
| fontSize   | 标题栏字体       | String   |                  | 0.16rem |
| showBack   | 是否显示回退按钮 | Boolean  |                  | true    |
| back       | 回退按钮         | Function |                  |         |
| rightClick | 右侧按钮事件     | Function |                  |         |

**插槽说明：**

​       Header自带两个slot,可自定义中间中间两侧内容:

搜索框左侧： name=header-left

搜索框右侧： name=header-right  （右侧slot与搜索确认按钮只可其一，如自定义搜索框右侧内容，则自实现确认搜索事件）

```
<Header title="首页" :alignStyle="alignStyle" :showBack="false">
	<span slot="header-left" class="iconfont iconrt iconBack" @click="searchConfrim"></span>
	<span slot="header-right" class="iconfont icontianjia icongo" @click="searchConfrim"></span>
</Header>
```



默认样式:

![image-20200721175447298](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200721175447298.png)

#### 使用实例

```vue
<Header title="进度查询" :alignStyle="alignStyle"  @back="onBack"></Header>
 alignStyle: {
    'position': 'fixed',
    'top': '0',
    'left': '0',
    'width': '100%',
    'font-weight': 'bold',
    'color': '#333',
    'background': '#fff',
    'z-index': '1000'
},
```

![image-20200721175814091](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200721175814091.png)

```
<Header title="首页" leftText="返回" rightIcon="icon1_mima" :alignStyle="alignStyle"></Header>
```

![image-20200820143840940](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200820143840940.png)