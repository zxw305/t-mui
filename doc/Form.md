# Form

#### 用法

```vue
import { Head, Form } from 'ftk-mui'
<Form
   :btnOption="btnOptionSet"
   :formData="formData"
   :initialData="initialData"
   @formSubmit="submit"
   ref="applyChild"
></Form>
```

Form属性说明如下：

| 属性        | 说明                      | 类型     | 可选值                                                       | 默认值       |
| :---------- | ------------------------- | -------- | ------------------------------------------------------------ | ------------ |
| btnOption   | 按钮设置                  | String   | text: "登录",   //  按钮text<br/>hidden: true,  // 是否显示按钮<br/>disabled: true, // 是否禁用按钮<br />btnStyle: {   // 按钮样式，同css<br/>    'background-color': 'red'<br/> }<br /> | 可参照Button |
| formData    | form dom内容              | Object   | 见下方详解                                                   |              |
| formSubmit  | 提交事件，返回dom输入内容 | Function |                                                              |              |
| initialData | 表单赋值\回显             | Object   |                                                              |              |



##### formData可选值

| 类型         | 说明       | 参数 | 图样                                                         |
| ------------ | ---------- | ---- | ------------------------------------------------------------ |
| ListItem     | list       | 如下 | ![image-20200728113708254](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728113708254.png) |
| Input        | 输入框     | 如下 | ![image-20200728114343475](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728114343475.png) |
| DoubleInput  | 双输入框   | 如下 | ![image-20200728144706852](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728144706852.png) |
| TripleInput  | 三输入框   | 如下 | ![image-20200728145923004](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728145923004.png) |
| Checkbox     | 多选框     | 如下 | ![image-20200728151041822](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728151041822.png) |
| Radio        | 单选       | 如下 | ![image-20200728162801402](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728162801402.png) |
| Switch       | 开关       | 如下 | ![image-20200728163258956](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728163258956.png) |
| Textarea     | 文本域     | 如下 | ![image-20200728163914393](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728163914393.png) |
| Picker       | 单列选择框 | 如下 | ![image-20200728164914136](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728164914136.png) |
| Datepicker   | 时间选择框 | 如下 | ![image-20200728165930747](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728165930747.png) |
| Regionpicker | 地区选择框 | 如下 | ![image-20200728171926667](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728171926667.png) |
| Validation   | 验证码     | 如下 | ![image-20200728173528963](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728173528963.png) |

例：

```
formData: [
	// 表单域1， 一般情况下一个表单域就可以
	{
		areaList: [
			{
				// 每一项 如input
			},
			{
				// 每一项 如checkbox
			}
		]
	},
	// 表单域2.......n
	{
		areaList: [
			{
				// 每一项
			}
		]
	}
]
```



###### ListItem参数

| 参数       | 类型       | 说明                                                         |
| ---------- | ---------- | ------------------------------------------------------------ |
| label      | String     | label名                                                      |
| alignStyle | Object     | label样式                                                    |
| titleNote  | String     | label备注                                                    |
| operList   | Object     | 右侧操作项，具体参数如下                                     |
|            | name       | 操作项名字                                                   |
|            | alignStyle | 操作项样式                                                   |
|            | nameClick  | 操作项操作事件                                               |
|            | icon       | 是否需要显示操作icon                                         |
|            | iconfont   | icon样式，支持图标见Icons组件，默认：iconarrow-left-copy     |
|            | status     | default 字体黑色 disabled 字体灰色 active 字体主题色 默认default |

例：

```vue
{
    type: "ListItem",
    label: "联系人",
    alignStyle: {
    	"font-weight": "bolder"
    },
    titleNote: '4895',
    operList: {
    	name: "清除内容",
   	 	alignStyle: {
    		"font-weight": "bolder"
    	},
    	nameClick: this.pickerCancel,
    	icon: true,
    // iconfont: "iconarrow-left-copy",
    // default 字体黑色 disabled 字体灰色 active 字体主题色  默认default
    // status: 'active',
    }
},
```

###### Input参数

| 参数        | 类型                    | 说明                                                         |
| ----------- | ----------------------- | ------------------------------------------------------------ |
| label       | String                  | label名                                                      |
| labelStyle  | Object                  | label样式                                                    |
| field       | String                  | 字段                                                         |
| required    | Boolean                 | 是否必输，默认false                                          |
| icon        | String                  | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle   | Object                  | 左侧图标样式                                                 |
| hidden      | Boolean                 | 是否隐藏，默认false                                          |
| fieldType   | String                  | 同input类型，如number等                                      |
| msgError    | String                  | 错误提示语                                                   |
| placeholder | String                  | placeholder                                                  |
| textAlign   | String（left \| right） | placeholder、内容对齐方式，默认右侧right                     |
| iconfont    | String                  | 右侧可操作性icon样式，支持图标见Icons组件，默认：iconarrow-left-copy |
| iconClick   | Function                | 右侧图标事件                                                 |
| onBlur      | Function                | 失焦事件                                                     |
| readonly    | Boolean                 | 只读默认 fasle                                               |
| validator   | function                | 自定义校验  return ''表示校验通过 否则需ruturn ‘具体错误’  见样例 |
| addOption   | Object                  | add 如maxlength等等设置，同input新加属性                     |

例：

```vue
 {
	type: "Input",
	label: "客户姓名",
	field: "testinput",
	required: true,
	icon: imgIcon,
	iconStyle: {
	// 'margin-left': '0.5rem'
	},
	// hidden: true,
	// 同input类型
	fieldType: "number",
	msgError: "请输入客户姓名",
	placeholder: "请输入客户姓名",
	iconfont: 'iconarrow-left-copy',
	// iconClick: this.testClick1,
	onBlur: this.testBlur,
	// textAlign: 'left',
	addOption: {
		maxlength: 3
	},
	// validator: this.testValidator,
	// disabled: "disabled"
	labelStyle: {
		'color': '#999999',
		'font-size': '0.12rem'
	}
}         

testValidator (value) {
    if (!/^1\d{10}$/.test(value)) {
        return '请输入正确的手机号'
     }
     // 如果无错误
     return ''
},
```

###### DoubleInput参数

| 参数       | 类型        | 说明                                   |
| ---------- | ----------- | -------------------------------------- |
| label      | String      | label名                                |
| icon       | String      | 左侧图标（目前为图片传入）不传时不显示 |
| iconStyle  | Object      | 左侧图标样式                           |
| required   | Boolean     | 是否必输，默认false                    |
| fieldArray | Array       | 双框设置[{},{}]                        |
|            | field       | 字段                                   |
|            | placeholder | placeholder                            |
|            | fieldType   | input类型                              |

例：

```vue
{
	type: "DoubleInput",
	label: "电话",
	icon: imgIcon,
	iconStyle: {
	// 'margin-left': '0.5rem'
	},
	// required: true,
	fieldArray: [
		{
			field: "doubleInputOne",
			placeholder: "区号",
			fieldType: "number"
		},
		{
			field: "doubleInputTwo",
			placeholder: "号码",
			fieldType: "number"
		}
	]
}
```

###### TripleInput参数

| 参数       | 类型        | 说明                |
| ---------- | ----------- | ------------------- |
| label      | String      | label名             |
| required   | Boolean     | 是否必输，默认false |
| fieldArray | Array       | 双框设置[{},{}]     |
|            | field       | 字段                |
|            | placeholder | placeholder         |
|            | fieldType   | input类型           |

例：

```vue
{
	type: "TripleInput",
	label: "省号码",
	// required: true,
	fieldArray: [
		{
			field: "TripleInputOne",
			placeholder: "省"
		},
		{
			field: "TripleInputTwo",
			placeholder: "区号"
		},
		{
			field: "TripleInputThree",
			placeholder: "号码"
		}
	]
}
```

###### Checkbox参数

| 参数       | 类型    | 说明                                                         |
| ---------- | ------- | ------------------------------------------------------------ |
| label      | String  | label名                                                      |
| field      | String  | 字段名                                                       |
| required   | Boolean | 是否必输，默认false                                          |
| icon       | String  | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle  | Object  | 左侧图标样式                                                 |
| msgError   | String  | 错误提示语                                                   |
| disabled   | Boolean | 是否禁用，默认false                                          |
| themeColor | String  | 背景主题色                                                   |
| typeProps  | Object  | 字典项对应关键词<br />typeProps:  {<br/>       label: "name",<br/>       value: "value"<br/> } |
| dataSource | Array   | 字典项  默认name,value                                       |
|            | name    | 选项名                                                       |
|            | value   | 选项值                                                       |

例：

```vue
{
	type: "Checkbox",
	label: "婚姻状况",
	required: true,
	icon: imgIcon,
	// iconStyle: {
	//   // 'margin-left': '0.5rem'
	// },
	msgError: "请选择婚姻状况",
	field: "testCheckbox",
	// disabled: true,
	// typeProps默认如下，如有其他对应可自定义设置
	// typeProps: {
    //    label: "name",
    //    value: "value"
    //},
	dataSource: [
		{
			name: "未婚",
			value: "1"
		},
		{
			name: "已婚",
			value: "2"
		}
	]
}
```

###### Radio参数

| 参数       | 类型     | 说明                                   |
| ---------- | -------- | -------------------------------------- |
| label      | String   | label名                                |
| field      | String   | 字段名                                 |
| required   | Boolean  | 是否必输，默认false                    |
| icon       | String   | 左侧图标（目前为图片传入）不传时不显示 |
| iconStyle  | Object   | 左侧图标样式                           |
| msgError   | String   | 错误提示语                             |
| disabled   | Boolean  | 是否禁用，默认false                    |
| themeColor | String   | 背景主题色                             |
| dataSource | Array    | 字典项  默认name,value                 |
|            | name     | 选项名                                 |
|            | value    | 选项值                                 |
| onClick    | Function | 单选点击事件 返回选择                  |

例：

```vue
{
	type: "Radio",
	label: "是否有驾照",
	field: "testRadio",
	// disabled: true,
	onClick: this.radioTest,
	msgError: "请输入时间",
	required: true,
	icon: imgIcon,
	// iconStyle: {
	//   // 'margin-left': '0.5rem'
	// },
	dataSource: [
		{
			name: "有",
			value: "1"
		},
		{
			name: "无",
			value: "2"
		}
	]
}
```

###### Switch参数

 说明： fasle 默认为关 true为开

| 参数      | 类型    | 说明                                                         |
| --------- | ------- | ------------------------------------------------------------ |
| label     | String  | label名                                                      |
| field     | String  | 字段名                                                       |
| required  | Boolean | 是否必输，默认false                                          |
| icon      | String  | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle | Object  | 左侧图标样式                                                 |
| color     | String  | switch颜色                                                   |
| option    | Object  | 开关对应话术<br />option: {<br/>     'true': '开',<br/>     'false': '关'<br/> } |

例：

```vue
{
	type: "Switch",
	label: "是否开启",
	// required: true,
	icon: imgIcon,
	// iconStyle: {
	//   // 'margin-left': '0.5rem'
	// },
	field: "testSwitch",
	// fasle 默认为关 true为开
	// 如果需显示对应话术
	// option: {
	//   'true': '开',
	//   'false': '关'
	// }
}
```

###### Textarea参数

| 参数        | 类型    | 说明                                   |
| ----------- | ------- | -------------------------------------- |
| label       | String  | label名                                |
| field       | String  | 字段名                                 |
| required    | Boolean | 是否必输，默认false                    |
| icon        | String  | 左侧图标（目前为图片传入）不传时不显示 |
| iconStyle   | Object  | 左侧图标样式                           |
| msgError    | String  | 错误提示语                             |
| placeholder | String  | placeholder                            |
| disabled    | Boolean | 是否禁用 默认false                     |
| maxlength   | String  | 最大输入长度                           |
| rows        | Number  | 行                                     |
| cols        | Number  | 列                                     |

例：

```vue
{
	type: "Textarea",
	label: "备注",
	// required: true,
	msgError: "请输入时间",
	placeholder: "zzzzzzzzzzz",
	field: "testTextarea",
	// icon: imgIcon,
	// iconStyle: {
	//   // 'margin-left': '0.5rem'
	// },
	// disabled: true,
	// 最大输入长度 默认不限制最大长度
	// maxlength: 15,
	rows: 2,
	cols: 15
}
```

###### Picker参数



| 参数         | 类型     | 说明                                                         |
| ------------ | -------- | ------------------------------------------------------------ |
| label        | String   | label名                                                      |
| field        | String   | 字段名                                                       |
| required     | Boolean  | 是否必输，默认false                                          |
| hidden       | Boolean  | 是否隐藏，默认false                                          |
| title        | String   | 标题                                                         |
| icon         | String   | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle    | Object   | 左侧图标样式                                                 |
| msgError     | String   | 错误提示语                                                   |
| onConfirm    | Function | 确认事件                                                     |
| confirmColor | String   | 确认按钮颜色                                                 |
| dataSource   | Array    | 字典项如：<br />dataSource: [<br/>       { name: "中国", value: 1323, text: "22" },<br/>       { name: "美国", value: 3333 },<br/>       { name: "日本", value: 4544 }<br/>  ] |
| iconfont     | String   | 右侧图标，可选值见Icons，右箭头为：iconarrow-left-copy       |
| isCanClick   | Boolean  | 右侧图标是否可点击选择 默认true                              |
| typeProps    | Object   | 字典项对应关键词默认如下：<br />typeProps:  {<br/>       label: "name",<br/>       value: "value"<br/> } |

例：

```vue
{
	type: "Picker",
	label: "国家",
	// required: true,
	// true为隐藏
	// hidden: true,
	title: "国家",
	onConfirm: this.testConfirm,
	field: "testPicker",
	icon: imgIcon,
	iconStyle: {
	// 'margin-left': '0.5rem'
	},
	dataSource: [
		{ name: "中国", value: 1323, text: "22" },
		{ name: "美国", value: 3333 },
		{ name: "日本", value: 4544 }
	],
	// iconfont: "iconarrow-left-copy",
	typeProps: {
		label: "name",
		value: "value"
	}
	// 箭头是否可点击
	// isCanClick: false,
}
```

###### Datepicker参数

| 参数         | 类型          | 说明                                                         |
| ------------ | ------------- | ------------------------------------------------------------ |
| label        | String        | label名                                                      |
| field        | String        | 字段名                                                       |
| dateMode     | String        | 选择器颗粒度 year、day、month、hour、minute、second 默认为day，即可选择到天（年月日） |
| required     | Boolean       | 是否必输，默认false                                          |
| hidden       | Boolean       | 是否隐藏，默认false                                          |
| title        | String        | 标题                                                         |
| icon         | String        | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle    | Object        | 左侧图标样式                                                 |
| msgError     | String        | 错误提示语                                                   |
| onConfirm    | Function      | 确认事件                                                     |
| confirmColor | String        | 确认按钮颜色                                                 |
| iconfont     | String        | 右侧图标，可选值见Icons，右箭头为：iconarrow-left-copy       |
| isCanClick   | Boolean       | 右侧图标是否可点击选择 默认true                              |
| startYear    | String/Number | 开始年份 默认1970                                            |
| endYear      | String/Number | 结束年份 默认2050                                            |

例：

```
{
	type: "Datepicker",
	label: "时间",
	field: "testDatepicker1",
	msgError: "请输入时间",
	startYear: '2030',
	endYear: '2050',
	// required: true,
	iconfont: "iconarrow-left-copy",
	// 箭头是否可点击
	// isCanClick: false,
	onConfirm: this.testConfirm,
	// 颗粒度:year、day、month、hour、minute、second  默认为day
	dateMode: "second"
}
```

###### Regionpicker参数

| 参数         | 类型     | 说明                                                         |
| ------------ | -------- | ------------------------------------------------------------ |
| label        | String   | label名                                                      |
| field        | String   | 字段名                                                       |
| hideArea     | Boolean  | 是否显示县区，默认false显示，true时只显示省市                |
| required     | Boolean  | 是否必输，默认false                                          |
| hidden       | Boolean  | 是否隐藏，默认false                                          |
| title        | String   | 标题                                                         |
| icon         | String   | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle    | Object   | 左侧图标样式                                                 |
| msgError     | String   | 错误提示语                                                   |
| onConfirm    | Function | 确认事件                                                     |
| confirmColor | String   | 确认按钮颜色                                                 |
| iconfont     | String   | 右侧图标，可选值见Icons，右箭头为：iconarrow-left-copy       |
| isCanClick   | Boolean  | 右侧图标是否可点击选择 默认true                              |
| typeProps    | Object   | 字典项对应关键词默认如下：<br />typeProps:  {<br/>       label: "text",<br/>       value: "value"<br/> } |
| dataSource   | Array    | 自传省市区联动项，如需看目前省市区联动格式可联系刘洪彬       |

例：

```vue
{
	type: "Regionpicker",
	iconfont: "iconarrow-left-copy",
	label: "省市区",
	// required: true,
	field: "testRegionpicker",
	confirmColor: 'red',
	// 是否显示县区 true不显示  false显示
	// hideArea: true,
	// dataSource: cityData,
	// 默认
	// typeProps: {
	//   label: 'text',
	//   value: 'value'
	// },
	onConfirm: this.testConfirm
}
```

###### Validation参数

| 参数        | 类型                     | 说明                                                         |
| ----------- | ------------------------ | ------------------------------------------------------------ |
| label       | String                   | label名                                                      |
| field       | String                   | 字段名                                                       |
| time        | Number                   | 倒计时秒数                                                   |
| required    | Boolean                  | 是否必输，默认false                                          |
| hidden      | Boolean                  | 是否隐藏，默认false                                          |
| placeholder | String                   | placeholder                                                  |
| icon        | String                   | 左侧图标（目前为图片传入）不传时不显示                       |
| iconStyle   | Object                   | 左侧图标样式                                                 |
| textAlign   | String （left \| right） | placeholder、内容对齐方式                                    |
| alignStyle  | Object                   | 字体颜色                                                     |
| inputStyle  | Object                   | input输入框样式                                              |
| beforeClick | Function                 | 点击获取验证码后倒计时前事件 return true则开始倒计时，否则return false停止 |
| onClick     | Function                 | 获取验证码事件                                               |

**备注：**

​		组件内另有重置方法resetTime，可如下调用：

​		this.$refs.child.$refs.testValidation[0].resetTime()

​			    child： form ref

​				testValidation：为field字段名

例：

```vue
{
	type: "Validation",
	// label: "验证码",
	field: "testValidation",
	time: 12,
	// disabled: true,
	placeholder: "请输入验证码",
	beforeClick: this.testVal,
	onClick: this.testClick1,
	// msgError: '请输入验证码',
	required: true,
	textAlign: 'left',
	// 字体颜色
	alignStyle: {
		color: "red"
	},
	inputStyle: {
		width: '1.71rem'
	},
	icon: imgIcon,
	iconStyle: {
		// 'margin-left': '0.5rem'
	}
}
testVal () {
    return true
},
testClick1(val) {
  console.log(2222222,val);
},
```



如上样例实现页面图：

![image-20200728174534037](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728174534037.png)



###### 使用样例1

```vue
<!-- form组件 -->
    <Form
      class="loginForm"
      :btnOption="btnOptionSet"
      :formData="formData"
      @formSubmit="submit"
      ref="loginChild"
    ></Form>
// button 配置
btnOptionSet: {
	text: '登录',
	btnStyle: {
		'position': 'inherit',
		'background-color': '#006DBB',
		'bottom': '2rem',
		'width': '100%',
		'border-radius': '0.2rem',
		'margin': '15% 0 0 0'
	}
},
// 页面项配置
      formData: [
        {
          areaList: [
            {
              type: 'Input',
              field: 'cellPhone',
              required: true,
              fieldType: 'number',
              msgError: '请输入手机号',
              placeholder: '请输入手机号',
              textAlign: 'left',
              icon: require('@assets/img/icon_phone.png'),
              iconStyle: {
                'height': '0.23rem',
                'width': '0.18rem',
                'margin-top': '0'
              },
              validator: this.loginValidator
            },
            {
              type: 'Validation',
              field: 'verifyCode',
              required: true,
              time: 60,
              textAlign: 'left',
              placeholder: '请输入验证码',
              msgError: '请输入验证码',
              alignStyle: {
                'color': '#006DBB'
              },
              icon: require('@assets/img/icon_vali.png'),
              iconStyle: {
                'height': '0.23rem',
                'width': '0.18rem',
                'margin-top': '0'
              },
              inputStyle: {
                width: '1.55rem'
              },
              onClick: this.getCode,
              beforeClick: this.beforeGetCode
            }
          ]
        }
      ],
 methods: {
    // 获取验证码前操作
    beforeGetCode () {
      // 验证手机号
      const phone = this.$refs.loginChild.form.cellPhone
      const testPhone = this.loginValidator(phone)
      if (testPhone === '') {
        return true
      } else {
        return false
      }
    },
    // 获取验证码
    getCode (data) {
      this.$store.dispatch({
        type: 'login/getVerifyCode',
        payload: {
          cellPhone: data.cellPhone,
          openid: this.openid
        },
        fail: () => {
          // 重置获取验证码
          this.$refs.loginChild.$refs.verifyCode[0].resetTime()
        }
      })
    },
    // 登录
    submit (data) {
      // 数据去空格操作
      let resData = deleteSpacing(data)
      this.$store.dispatch({
        type: 'login/login',
        payload: {
          ...resData,
          openid: this.openid
        },
        success: (data) => {
          this.$router.push({path: '/dashboard'})
        }
      })
    }
  }
```

实现图：

![image-20200728174946579](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200728174946579.png)

###### 使用样例2

```vue
<!-- form组件 -->
    <div class="form-apply">
      <Form
        :btnOption="btnOptionSet"
        :formData="formData"
        :initialData="initialData"
        @formSubmit="submit"
        ref="applyChild"
      ></Form>
    </div>
// button 配置
      btnOptionSet: {
        text: '确认开卡',
        btnStyle: {
          'background-color': '#006DBB'
        }
      },
 // 页面项配置
      formData: [
        {
          areaList: [
            {
              type: 'Input',
              label: '姓名',
              field: 'custName',
              disabled: true
              // required: true
            },
            {
              type: 'Input',
              label: '身份证号',
              field: 'idNo',
              disabled: true
              // required: true
            },
            {
              type: 'Picker',
              label: '职业',
              required: true,
              field: 'occupationDesc',
              msgError: '请选择职业',
              placeholder: '请选择职业',
              iconfont: 'iconarrow-left-copy',
              confirmColor: '#006DBB',
              dataSource: occupationList,
              typeProps: {
                label: 'Occupation',
                value: 'OctId'
              },
              onConfirm: this.confirmOccupation
            },
            {
              type: 'Picker',
              label: '职业备注',
              hidden: true,
              required: true,
              field: 'occupationDetail',
              dataSource: additionalOcu,
              iconfont: 'iconarrow-left-copy',
              confirmColor: '#006DBB',
              msgError: '请选择职业',
              placeholder: '请选择职业'
            },
            {
              type: 'Input',
              label: '工作单位',
              required: true,
              field: 'orgEmp',
              msgError: '请输入工作单位',
              placeholder: '请输入工作单位',
              validator: this.orgEmpValidator
            },
            {
              type: 'Regionpicker',
              label: '省市区',
              required: true,
              field: 'addrDesc',
              dataSource: cityData,
              iconfont: 'iconarrow-left-copy',
              confirmColor: '#006DBB',
              msgError: '请选择工作单位省市区',
              placeholder: '请选择工作单位省市区',
              onConfirm: this.addrDescrConfirm
            },
            {
              type: 'Input',
              label: '联系地址',
              required: true,
              field: 'addrDetail',
              msgError: '请输入具体联系地址',
              placeholder: '请输入具体联系地址',
              validator: this.orgEmpValidator
            },
            {
              type: 'Input',
              label: '还款银行卡号',
              field: 'acctNo',
              required: true,
              msgError: '请输入还款银行卡号',
              placeholder: '请输入还款银行卡号'
            },
            {
              type: 'Input',
              label: '还款卡绑定手机号',
              field: 'acctNoPhone',
              required: true,
              fieldType: 'number',
              msgError: '请输入还款卡绑定手机号',
              placeholder: '请输入还款卡绑定手机号',
              validator: this.loginValidator
            },
            {
              type: 'Validation',
              label: '验证码',
              field: 'MessageCode',
              required: true,
              time: 60,
              textAlign: 'left',
              placeholder: '请输入验证码',
              msgError: '请输入验证码',
              alignStyle: {
                'color': '#006DBB'
              },
              onClick: this.getCode,
              beforeClick: this.beforeGetCode
            },
            {
              type: 'Input',
              label: '交易密码',
              required: true,
              fieldType: 'text',
              field: 'PwdResultTemp',
              msgError: '请输入6位交易密码',
              placeholder: '请输入6位交易密码',
              readonly: true,
              onBlur: this.keyBoardComBlur.bind('', 'PwdResultTemp', 'PwdResult')
            },
            {
              type: 'Input',
              label: '确认交易密码',
              required: true,
              fieldType: 'text',
              field: 'PwdResultConfirmTemp',
              msgError: '请确认6位交易密码',
              placeholder: '请确认6位交易密码',
              readonly: true,
              onBlur: this.keyBoardComBlur.bind('', 'PwdResultConfirmTemp', 'PwdResultConfirm')
            }
          ]
        }
      ],
methods: {
    // SP录入用户信息回显
    initOpenCard () {
      const {openid, cellPhone} = this
      this.$store.dispatch({
        type: 'applyOpencard/GETINFOBYPHONE',
        payload: {
          openid,
          cellPhone
        },
        success: (data) => {
          // 取消loading
          this.loadingVisible = false
          // 保存以及显示回显数据
          this.basicInfo = data
          // 如果号码不存在，则不回显号码 防止本页面赋值影像页面渲染
          if (!data.acctNoPhone) {
            delete data.acctNoPhone
          }
          this.initialData = Object.assign({}, this.initialData, data)
          // 初始化数字加密键盘
          this.initKeyBoard(data.Random)
        },
        fail: () => {
          // 取消loading
          this.loadingVisible = false
        }
      })
    },
    // 获取开卡验证码前操作
    beforeGetCode () {
      // 验证手机号
      const phone = this.$refs.applyChild.form.acctNoPhone
      const testPhone = this.loginValidator(phone)
      if (testPhone === '') {
        return true
      } else {
        return false
      }
    },
    // 获取开卡验证码
    getCode (data) {
      this.$store.dispatch({
        type: 'applyOpencard/getCreateAcctCode',
        payload: {
          acctNoPhone: data.acctNoPhone,
          applSeq: this.basicInfo.applSeq
        }
      })
    },
    /*
    * 交易密码、确认交易密码公用
    * filed 绑定字段名
    * setField 需要赋值保存的state名
    * value 广义上的输入值
    */
    keyBoardComBlur (filed, setField, value) {
      // 获取加密数据
      let encryptData = this.getEncrypt(filed)
      // 保存加密数据
      if (encryptData) {
        this[setField] = encryptData
        this.initialData = Object.assign({}, this.initialData, {[filed]: '******'})
      } else {
        this.initialData = Object.assign({}, this.initialData, {[filed]: ''})
        this.toast('加密错误')
      }
    },
    // 职业确认
    confirmOccupation (data) {
      // 保存选择
      this.occupation = data.OctId
      // 不便分类的其他从业人员时 需添加职业备注
      this.formData[0].areaList.map(item => {
        if (item.field === 'occupationDetail') {
          item.hidden = data.OctId !== '82'
        }
      })
    },
    // 省市区
    addrDescrConfirm (data) {
      // 只需传县/区
      this.addr = data.area.value
    },
    // 工作单位校验
    orgEmpValidator (value) {
      if (value.length > 50) {
        return '输入长度不能大于50'
      }
      // 如果无错误
      return ''
    },
    // 申请开卡
    submit (data) {
      // 数据去空格操作
      let resData = deleteSpacing(data)
      const {PwdResult, PwdResultConfirm, addr, occupation, openid, codeMesage} = this
      // 接口请求
      this.$store.dispatch({
        type: 'applyOpencard/createAcctNo',
        payload: {
          ...resData,
          ...codeMesage,
          openid,
          PwdResult, // 交易密码
          PwdResultConfirm, // 确认交易密码
          addr, // 省市区，只需传区
          occupation
        },
        success: () => {
          this.$router.push({path: '/opencardResult'})
        }
      })
    },
    // 回退
    back () {
      this.$router.goBack()
    }
  }
```

![image-20200810171912274](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200810171912274.png)