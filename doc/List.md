# List

#### 用法

```vue
import { List } from 'ftk-mui'
<List :dataSource="listSource" :alignStyle="listAlignStyle"></List>
listAlignStyle: {
 padding: 0
},
```

List的属性说明如下：

| 属性       | 说明           | 类型  | 可选值 | 默认值 |
| :--------- | -------------- | ----- | ------ | ------ |
| dataSource | list数组列表   | Array | 必传   |        |
| alignStyle | list自定义样式 |       |        |        |

除以list形式引用外，另支持单个listItem引用，

```
import { ListItem } from 'ftk-mui'
<ListItem></ListItem>
```

ListItem属性说明如下：

| 属性       | 说明               | 类型     | 可选值                                                       | 默认值 |
| :--------- | ------------------ | -------- | ------------------------------------------------------------ | ------ |
| titleName  | list左侧标题       | String   |                                                              |        |
| titleNote  | list左侧标题备注   | String   |                                                              |        |
| hidden     | 是否隐藏本行       | Boolean  |                                                              |        |
| alignStyle | 标题自定义样式     | Object   |                                                              |        |
| noBorder   | 是否需要底部border | Boolean  |                                                              |        |
| operList   | 右侧项             | Object   |                                                              |        |
|            | alignStyle         | Object   | 右侧标题自定义样式                                           |        |
|            | name               | String   | 右侧标题                                                     |        |
|            | nameClick          | Function | 标题点击事件                                                 |        |
|            | status             | String   | default 字体黑色 disabled 字体灰色 active 字体主题色 默认default |        |
|            | iconfont           | iconfont | 右侧图标（具体见Icons），默认右侧箭头                        |        |
|            | icon               | Boolean  | 是否使用右侧箭头                                             |        |
|            | iconClick          | Function | 右侧箭头点击事件                                             |        |
|            | iconStyle          | Object   | 右侧icon样式                                                 |        |



List示例代码：

```vue
listSource: [
        {
          titleName: "全部业务",
          // 是否隐藏本行
          hidden: false,
          titleNote: "4895",
          // alignStyle: {
          //   "font-weight": "bolder"
          // },
          operList: {
            //    alignStyle: {
            // "font-weight": "bolder"
            //    },
            name: "待完善",
            nameClick: this.testClick,
            // default 字体黑色 disabled 字体灰色 active 字体主题色  默认default
            status: "active",
            // 是否使用箭头
            icon: true,
            iconClick: this.testClick
          }
        },
        {
          titleName: "全部业务2",
          // titleNote: '4895',
          operList: {
            // default 字体黑色 disabled 字体灰色 active 字体主题色  默认default
            status: "active",
            // 是否使用箭头
            icon: true,
            iconClick: this.testClick
          }
        },
        {
          titleName: "全部业务3",
          // titleNote: '4895',
          operList: {
            name: "待完善",
            nameClick: this.testClick
          }
        },
        {
          titleName: "全部业务4",
          alignStyle: {
            "font-weight": "bolder"
          },
          operList: {
            name: "待完善",
            alignStyle: {
              "font-weight": "bolder"
            },
            nameClick: this.testClick
          }
        },
        {
          titleName: "全部业务5",
          noBorder: true,
          // titleNote: '4895',
          operList: {
            // default 字体黑色 disabled 字体灰色 active 字体主题色  默认default
            status: "active",
            // 是否使用箭头
            icon: true,
            iconClass: "icontianjia",
            iconClick: this.testClick1,
            iconStyle: {
              color: "red"
            }
          }
        }
      ]
```

样式:

![image-20200819150851797](C:\Users\Administrator.SKY-20170926FUA\AppData\Roaming\Typora\typora-user-images\image-20200819150851797.png)