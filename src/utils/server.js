import axios from 'axios'
const Server ={
	uploadingFile(data, url){
		return axios(url, {
			method: 'post',
			data: data,
			// 修改请求头才能传文件
			headers: {'Content-Type': 'multipart/form-data'}
		})
	}
}
export default Server;
