/**
 * Created by Administrator on 2019/3/29.
 */
import Vue from 'vue'

// 加载所有组件
const modulesFiles = require.context('./', true, /\.vue$/)
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

// 注册所有组件
Object.keys(modules).forEach(item => {
  Vue.component(modules[item].name, modules[item])
})